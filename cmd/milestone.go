package cmd

import (
	"fmt"
	"os"
)

func parseMilestone(c *command) error {
	c.name = os.Args[0]
	c.description = "description"
	c.args = os.Args[2:]
	fmt.Println("There are", len(c.args), "milestone args:\n", c.args)
	return nil
}
